"use client";

import { useEffect, useState } from "react";

export default function Home() {
    const [inputValue, setInputValue] = useState("");
    const [result, setResult] = useState<any[]>([]);
    const [error, setError] = useState<boolean>(false);

    const numberValidation = (e: string) => {
        setInputValue(e);
    };

    const handleTriangle = (val: string) => {
        const arr = val.split("");

        const data = arr.map((el: string, index: number) => {
            return `${el}${"0".repeat(index + 1)}`;
        });
        setResult(data);
    };

    const handleOdd = (val: string) => {
        const arr = [];
        const max = parseInt(val);

        for (let index = 0; index < max; index++) {
            if ((index + 1) % 2 == 1) {
                arr.push(index + 1);
            }
        }

        setResult(arr);
    };

    const isPrime = (num: number) => {
        if (num < 2) return false;

        for (let index = 2; index < num; index++) {
            if (num % index === 0) {
                return false;
            }
        }

        return true;
    };

    const handlePrimeNumber = (val: string) => {
        const arr = [];
        const max = parseInt(val);

        for (let index = 0; index < max; index++) {
            if (isPrime(index + 1)) {
                arr.push(index + 1);
            }
        }

        setResult(arr);
    };

    // this effect used to validate the input number
    useEffect(() => {
        console.log(inputValue);
        const regex = /[^0-9]/g;
        if (inputValue.match(regex) !== null) {
            setError(true);
        } else {
            setError(false);
        }
    }, [inputValue]);

    return (
        <div className="w-screen h-screen">
            <div className="max-w-5xl p-12 m-auto space-y-4">
                <input
                    type="text"
                    value={inputValue}
                    onChange={(e) => setInputValue(e.target.value)}
                    placeholder="Input Angka"
                    className={`focus:outline-none border  ${
                        error ? "border-red-500" : "focus:border-blue-400"
                    }`}
                />
                {error && (
                    <div className="text-xs text-red-500">
                        please input numbers only.
                    </div>
                )}
                <div className="flex gap-4">
                    <button
                        disabled={error}
                        className={`bg-gray-100 border p-1 ${
                            error ? "bg-gray-300" : ""
                        }`}
                        onClick={() => handleTriangle(inputValue)}
                    >
                        Ganerate segitiga
                    </button>
                    <button
                        disabled={error}
                        className={`bg-gray-100 border p-1 ${
                            error ? "bg-gray-300" : ""
                        }`}
                        onClick={() => handleOdd(inputValue)}
                    >
                        Ganerate Bilangan Ganjil
                    </button>
                    <button
                        disabled={error}
                        className={`bg-gray-100 border p-1 ${
                            error ? "bg-gray-300" : ""
                        }`}
                        onClick={() => handlePrimeNumber(inputValue)}
                    >
                        Ganerate Bilangan Prima
                    </button>
                </div>
                <div className="text-2xl font-bold">Result</div>
                <div>
                    {result.map((el: any, index: number) => {
                        return <div key={index}>{el}</div>;
                    })}
                </div>
            </div>
        </div>
    );
}
